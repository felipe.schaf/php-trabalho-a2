<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="../css/style.css">
<title>Registro de Ponto</title>
</head>
<body>

<div class="container">
    <h1>Registro de Ponto</h1>
    <p>Data atual: <?php echo date("d/m/Y"); ?></p>
    <p>Horário atual: <span id="relogio"></span></p>
    
    <form id="registroForm" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="matricula" class="form-control">Matrícula do Funcionário:</label>
        <input type="text" id="matricula" name="matricula" class="form-control" required>
        <button type="submit" name="acao" value="buscarFuncionario" id="buscarFuncionario" class="btnuncionario">Buscar Funcionário</button>
        
        <div class="resultado">
            <?php
            $funcionario = null;

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['acao']) && $_POST['acao'] === 'buscarFuncionario') {
                if (!empty($_POST['matricula'])) {
                    include 'funcoes.php';
                    $pdo = conectarBanco();
                    $matricula = $_POST['matricula'];
                    $funcionario = buscarFuncionarioPorMatricula($pdo, $matricula);
                    if ($funcionario) {
                        echo '<b>Funcionário encontrado: ' . $funcionario['nome'] . '</b>';
                        echo '<script>';
                        echo 'document.getElementById("justificativa").disabled = false;';
                        echo 'document.getElementById("registrarPonto").disabled = false;';
                        echo '</script>';
                    } else {
                        echo '<script>alert("Funcionário não encontrado.");</script>';
                    }
                } else {
                    echo '<script>alert("Por favor, informe a matrícula do funcionário.");</script>';
                }
            }

            if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['acao']) && $_POST['acao'] === 'registrarPonto') {
                if (!empty($_POST['matricula'])) {
                    include 'funcoes.php';
                    $pdo = conectarBanco();
                    $matricula = $_POST['matricula'];
                    $justificativa = $_POST['justificativa'];
                    $resultado = registrarPonto($pdo, $matricula, $justificativa);
                    if ($resultado) {
                        echo '<script>alert("Ponto registrado com sucesso!");</script>';
                    } else {
                        echo '<script>alert("Ocorreu um erro ao registrar o ponto.");</script>';
                    }
                }
            }
            ?>
        </div>
        <br>
        <br>
        <label for="justificativa" class="form-control">Justificativa:</label>
        <input type="text" id="justificativa" name="justificativa" class="form-control-justificativa-input" style="padding: 50px" <?php if (!$funcionario) echo 'disabled'; ?>><br><br>
        
        <button type="submit" name="acao" value="registrarPonto" id="registrarPonto" class="btn-registrar-ponto" <?php if (!$funcionario) echo 'disabled style="background-color: #dbdbdb"'; ?>>Registrar Ponto</button>

    </form>
    <br>  
    <br>       
    <div>
        <form action="index.php" method="get">
            <button type="submit" class="btn" style="background-color: #9cb6ff">Voltar para a tela inicial</button>
        </form>
    </div>
</div>



<script>
function atualizarRelogio() {
    var agora = new Date();
    var hora = agora.getHours();
    var minuto = agora.getMinutes();
    var segundo = agora.getSeconds();
    
    hora = hora < 10 ? "0" + hora : hora;
    minuto = minuto < 10 ? "0" + minuto : minuto;
    segundo = segundo < 10 ? "0" + segundo : segundo;
    
    document.getElementById('relogio').textContent = hora + ':' + minuto + ':' + segundo;
}

setInterval(atualizarRelogio, 1000);
atualizarRelogio();

</script>

</body>
</html>

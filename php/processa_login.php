<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once('funcoes.php');

    $usuario = $_POST['usuario'];
    $senha = $_POST['senha'];

    $resultado = realizarLogin($usuario, $senha);

    if ($resultado) {
        header('Location: index.php');
        exit; 
    } else {

        echo '<script>';
        echo 'alert("Usuário ou senha incorretos.");';
        echo 'window.location.href = "login.php";'; // Redireciona após o OK do alerta
        echo '</script>';
        exit;
    }
}
?>

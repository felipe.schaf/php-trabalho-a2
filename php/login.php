<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">

    <title>PHPONTO - Login</title>
</head>
<body>

<div class="container">
    <h1>PHPonto</h1>
    <form action="processa_login.php" method="post">
        <div class="form-control">
            <label for="usuario">Usuário:</label>
            <input type="text" id="usuario" name="usuario" class="campo-de-entrada">
        </div>
        <div class="form-control">
            <label for="senha">Senha:</label>
            <input type="password" id="senha" name="senha" class="campo-de-entrada">
        </div>
        <div class="form-control">
            <button type="submit" class="btn-enviar">Entrar</button>
        </div>
    </form>
</div>
<div class="caixa-dica">
    <b>Para facilitar a correcao:</b>
    <br>
    Login: admin
    <br>
    Senha: 123
</div>
</body>

</html>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Formulário de Cadastro de Funcionário</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

        <div class="container-cadastro">
        <h1  class="btn-cadastro">Cadastro de Funcionário</h1>
            <form action="processa_cadastro.php" method="post">
                <label class="label-cadastro" for="nome">Nome:</label><br>
                <input type="text" id="nome" name="nome" required><br><br>
                
                <label class="label-cadastro" for="cpf">CPF:</label><br>
                <input type="text" id="cpf" name="cpf" required><br><br>
                
                <label class="label-cadastro" for="data_nascimento">Data de Nascimento:</label><br>
                <input type="date" id="data_nascimento" name="data_nascimento" required><br><br>
                
                <label class="label-cadastro" for="matricula">Matrícula:</label><br>
                <input type="text" id="matricula" name="matricula" required><br><br>
                
                <label class="label-cadastro" for="cargo">Cargo:</label><br>
                <input type="text" id="cargo" name="cargo" required><br><br>
                
                <button type="submit" value="Cadastrar">Cadastrar</button>
            </form>

            <div>
                <form action="index.php" method="get">
                    <button type="submit" class="btn-voltar" style="background-color: #9cb6ff">Voltar para a tela inicial</button>
                </form>
            </div>
        </div>

</body>
</html>

<?php
function criarDatabase() {
    $hostname = 'localhost';
    $username = 'root';
    $password = '';

    try {
        $pdo = new PDO("mysql:host=$hostname", $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $pdo->exec("DROP DATABASE IF EXISTS Ponto");

        $sql = "CREATE DATABASE Ponto";
        $pdo->exec($sql);

        $pdo->exec("USE Ponto");

        $sql = "CREATE TABLE IF NOT EXISTS funcionario (
                    id INT(11) AUTO_INCREMENT PRIMARY KEY,
                    nome VARCHAR(100) NOT NULL,
                    cpf VARCHAR(14) NOT NULL,
                    data_nascimento DATE NOT NULL,
                    matricula VARCHAR(20) NOT NULL,
                    cargo VARCHAR(100) NOT NULL
                )";
        $pdo->exec($sql);

        $sql = "CREATE TABLE IF NOT EXISTS usuario (
                    id INT(11) AUTO_INCREMENT PRIMARY KEY,
                    usuario VARCHAR(100) NOT NULL,
                    senha VARCHAR(100) NOT NULL
                )";
        $pdo->exec($sql);

        $sql = "CREATE TABLE IF NOT EXISTS registros (
                    id INT(11) AUTO_INCREMENT PRIMARY KEY,
                    matricula INT(11) NOT NULL,
                    data_registro DATE NOT NULL,
                    justificativa VARCHAR(255)
                )";
        $pdo->exec($sql);

        $sql = "INSERT INTO usuario (usuario, senha)
                VALUES ('admin', '123')";
        $pdo->exec($sql);

    } catch (PDOException $e) {
        die("Erro ao executar o script: " . $e->getMessage());
    }
}
?>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
     require_once('funcoes.php');

    $nome = $_POST['nome'];
    $cpf = $_POST['cpf'];
    $data_nascimento = $_POST['data_nascimento'];
    $matricula = $_POST['matricula'];
    $cargo = $_POST['cargo'];


    $resultado = criarfuncionario($nome, $cpf, $data_nascimento, $matricula, $cargo);

    if ($resultado) {

        echo '<script>';
        echo 'alert("Funcionário cadastrado com sucesso!");';
        echo 'window.location.href = "index.php";'; 
        echo '</script>';

    } else {
        echo '<script>';
        echo 'alert("Erro ao cadastrar!");';
        echo 'window.location.href = "cadastrar_func_index.php";'; 
        echo '</script>';
    }
}
?>

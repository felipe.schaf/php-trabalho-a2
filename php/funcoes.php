<?php

function realizarLogin($usuario, $senha) {
    $pdo = conectarBanco();

    if (!$pdo) {
        return false;
    }

    try {

        $sql = "SELECT * FROM usuario WHERE usuario = :usuario AND senha = :senha";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':usuario', $usuario, PDO::PARAM_STR);
        $stmt->bindParam(':senha', $senha, PDO::PARAM_STR); 
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            return true; 
        } else {
            return false; 
        }
    } catch (PDOException $e) {
        echo "Erro ao realizar login: " . $e->getMessage();
        return false;
    }
}

function criarFuncionario($nome, $cpf, $data_nascimento, $matricula, $cargo) {
    $pdo = conectarBanco();

    if (!$pdo) {
        return false; 
    }

    try {
        $sql = "INSERT INTO funcionario (nome, cpf, data_nascimento, matricula, cargo) 
                VALUES (:nome, :cpf, :data_nascimento, :matricula, :cargo)";
        $stmt = $pdo->prepare($sql);
        
        $stmt->bindParam(':nome', $nome);
        $stmt->bindParam(':cpf', $cpf);
        $stmt->bindParam(':data_nascimento', $data_nascimento);
        $stmt->bindParam(':matricula', $matricula);
        $stmt->bindParam(':cargo', $cargo);
        
        $stmt->execute();

        return true; 
    } catch (PDOException $e) {
        echo "Erro ao criar funcionário: " . $e->getMessage();
        return false;
    }
}

function conectarBanco() {
    $hostname = 'localhost';
    $username = 'root';
    $password = '';
    $dbname = 'ponto';
    
    try {
        $pdo = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch (PDOException $e) {
        die("Erro na conexão com o banco de dados: " . $e->getMessage());
    }
}

function buscarFuncionarioPorMatricula($pdo, $matricula) {
    try {
        $sql = "SELECT * FROM funcionario WHERE matricula = :matricula";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':matricula', $matricula, PDO::PARAM_STR);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo "Erro ao buscar funcionário por matrícula: " . $e->getMessage();
        return false;
    }
}


function registrarPonto($pdo, $matricula, $justificativa) {
    try {
        $dataHoraAtual = date('Y-m-d H:i:s');
        $query = "INSERT INTO registros (matricula, data_registro, justificativa) VALUES (?, ?, ?)";
        $stmt = $pdo->prepare($query);
        $stmt->execute([$matricula, $dataHoraAtual, $justificativa]);
        return true; 
    } catch (PDOException $e) {
        echo "Erro ao registrar ponto: " . $e->getMessage();
        return false;
    }
}
?>
